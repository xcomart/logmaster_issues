# Privacy Policy

Effective date: April 29, 2019

Dennis Soungjin Park operates the LogMaster macOS application.

LogMaster does **NOT** collect any of your private data.

LogMaster saves critical user data to local system with 3-DES encryption, and does **NOT** collect data for any purpose.

## Changes To This Privacy Policy

We may update our Privacy Policy in future, but will **NOT** change policy of collecting any of your data.

We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.

You are advised to review this Privacy Policy periodically for any changes.
Changes to this Privacy Policy are effective when they are posted on this page.

Contact Us

If you have any questions about this Privacy Policy, please contact us:

By email: xcomart@gmail.com
