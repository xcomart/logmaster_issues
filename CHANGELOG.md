# LogMaster Change Logs


## Version 1.4

**[Bug Fixes]**
1. Crashes when file transfer - fixed.
2. SSH tunnel attempt even if it's disabled - fixed.

**[Enhancements]**
1. SFTP transfer rate increased slightly.
2. xterm line drawing sequence processing bug fixed.
